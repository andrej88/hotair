extends Node

const MUSIC_IDX = 1
const SOUND_IDX = 2
const CURRENT_BALLOON_IDX = 3
const TOTAL_BALLOONS_IDX = 4
const PREV_100_SCORES_IDX = 5
const HIGH_SCORE_IDX = 6
const TOTAL_GAMES_PLAYED_IDX = 7

var records = {"Music":true, "Sound":true, "Current Balloon":1, "Total Balloons":3, "Previous Scores":[], "Best Score":0, "Games Played":0}

var save_path = "user://hotair.sav"

func _ready():
	
	var f = File.new()
	if !f.file_exists(save_path):
		var err = f.open_encrypted_with_pass(save_path, File.WRITE, OS.get_unique_ID())
		if f.is_open():
			f.store_var(records)
			f.close()
		else:
			print("Error: File not open")


func get_item(item):
	
	var record
	var f = File.new()
	var err = f.open_encrypted_with_pass(save_path, File.READ, OS.get_unique_ID())
	if f.is_open():
		for i in range(item):
			record = f.get_var()
		f.close()
	return record


func set_item(item, value):
	
	var f = File.new()
	var err = f.open_encrypted_with_pass(save_path, File.READ_WRITE, OS.get_unique_ID())
	if f.is_open():
		for i in range(item-1):
			var v = f.get_var()
			f.store_var(v)
		f.store_var(value)
		f.close()