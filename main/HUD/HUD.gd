extends Control

var screen_size
var move = get_node("Move")

func _ready():
	
	screen_size = get_viewport_rect().size
	
	move = get_node("Move")
	
	move.scale(Vector2(screen_size.x, screen_size.y))