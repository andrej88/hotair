
extends Panel

# member variables here, example:
# var a=2
# var b="textvar"

func _ready():
	
	get_node("Resume").connect("pressed",self,"_on_resume_pressed")


func _on_resume_pressed():
	
	get_tree().set_pause(false)
	hide()

