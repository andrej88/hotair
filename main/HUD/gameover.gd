
extends Label

const DELAY = 60
var time_til_respawn = 0

func _ready():
	time_til_respawn = 0


func _fixed_process(delta):

	time_til_respawn += 1
	
	if time_til_respawn >= DELAY:
		set_process_input(true)
		set_fixed_process(false)


func _input(event):
	
	if ((event.type == InputEvent.SCREEN_TOUCH) or (event.type == InputEvent.KEY and event.unicode == KEY_M + 32)):
		var main = get_parent().get_parent().get_parent()
		var balloon = main.get_node("Balloon")
		
		for i in main.get_node("Hazards").get_children():
			for j in i.get_children():
				j.queue_free()
		
		balloon.set_name("Old Balloon")
		balloon.queue_free()
		
		main._ready()
		main.get_node("Menu").show()
		main.get_node("Menu").set_process_input(true)
		main.get_node("HUD/Score").hide()
		set_process_input(false)
		get_parent().hide()

