extends Control


var screen_size
var layer_0 = get_node("0")
var layer_1 = get_node("1")

var layer_1_speed

var scaled = false


func _ready():
	
	screen_size = get_viewport_rect().size
	
	layer_0 = get_node("0")
	layer_1 = get_node("1")
	
	layer_1_speed = screen_size.y * 0.01
	
	initialize_layers()
	
	set_process(true)


func initialize_layers():
	
	if !scaled:
		var layers = get_children()
		for i in layers:
			if i extends Node2D:
				scale_to_screen_width(i)
		scaled = true
	
	layer_0.set_pos(Vector2(screen_size.x, screen_size.y) * 0.5)
	
	var layer_1_size = layer_1.get_scale() * layer_1.get_node("0").get_texture().get_size()
	var layer_1_y = (-0.5 * layer_1_size.y * layer_1.get_child_count()) + screen_size.y
	layer_1.set_pos(Vector2(screen_size.x * 0.5, layer_1_y))
	print(str(layer_1_y))


func scale_to_screen_width(node):
	var width = get_width(node)
	var scale = screen_size.x / width
	node.scale(Vector2(scale, scale))


func get_width(n):
	if n extends Sprite:
		return n.get_texture().get_size().x
	elif n.get_child_count() > 0:
		return get_width(n.get_child(0))
	else:
		print("Error: No sprite children")
		return 1


func _process(delta):
	var layer_1_old_pos = layer_1.get_pos()
	layer_1.set_pos(layer_1_old_pos + Vector2(0, layer_1_speed * delta))