
extends Control

var bg_scroll_speed = 1
var screen_size
var new_bg_size

func _ready():
	
	screen_size = get_viewport_rect().size
	var og_bg_size = get_node("1").get_texture().get_size()
	
	# Resize nodes 1, 2, 3 so that bg is the height of the screen
	
	
	#for i in range(3):
	#	get_node(str(i+1)).set_size(Vector2(screen_size.y, screen_size.y))
	
	new_bg_size = get_node("1").get_size()
	
	# Lay out the nodes one on top of the other
	get_node("1").set_pos(Vector2(0, 0))
	get_node("2").set_pos(Vector2(0, floor(-1 * new_bg_size.y)))
	get_node("3").set_pos(Vector2(0, floor(-2 * new_bg_size.y)))
	
	set_fixed_process(true)


func _fixed_process(delta):
	
	for i in range(3):
		
		var current_bg = get_node(str(i+1))
		var initial_pos = current_bg.get_pos()
		get_node(str(i+1)).set_pos(Vector2(initial_pos.x, initial_pos.y + bg_scroll_speed))
		
		if (current_bg.get_pos().y > screen_size.y):
			current_bg.set_pos(Vector2(initial_pos.x, (-2 * new_bg_size.y) + screen_size.y))

