extends KinematicBody2D

var velocity_x = 0
var velocity_multiplier = 1
const VELOCITY_FIX = 405

const LEFT = -1
const RIGHT = 1
var direction = RIGHT
var has_changed_direction = false
var is_moving

const ACCEL = 0.8
const BRAKE = 2
const DECEL = 0.1
const TOP_SPEED = 100000

const BOUNCINESS = 0.5    # Wall bounciness, a number between 0 and 1 (0 = not bouncy, 1 = perfectly bouncy)

const BALLOON_WIDTH_AS_PCT = 0.25

var balloon_size = Vector2(128,180)
var screen_size
var radius = 64

# rotation values in radians
export var rotation = 0
var rot_prev = 0
var rot_delta = 0
const ROT_ACCEL = 2.5 / 60.0
const ROT_DECEL = 1.0 / 60.0
const MAX_ROT = 3.141593 / 4

var save_path = "user://hotair.sav"


func _ready():
	
	screen_size = get_viewport_rect().size
	
	radius = BALLOON_WIDTH_AS_PCT * screen_size.x * 0.25
	
	velocity_x = 0
	rotation = 0
	direction = RIGHT
	
	velocity_multiplier = screen_size.x / VELOCITY_FIX
	
	set_fixed_process(true)
	set_process_input(true)


func _fixed_process(delta):
	
	handle_movement()


func _input(event):
	
	if event.type == InputEvent.KEY:
		
		if event.unicode == KEY_R + 32:
			_ready()
	
	if event.type == InputEvent.SCREEN_TOUCH:
		
		is_moving = event.pressed
		
		if event.pressed:
			has_changed_direction = false
		
		# Comment this out to enable single-press control scheme:
		if (event.x < screen_size.x * 0.5):
			direction = LEFT
		else:
			direction = RIGHT


####################
# Helper Functions #
####################

func handle_movement():

	#var pos = get_parent().get_node("Balloon").get_pos()
	var pos = get_pos()
	
	#hm_keyboard(pos)
	hm_mobile(pos)
	
	# slow down and bounce off screen edge ##### WALL COLLISION DETECTION IS HERE
	if ((pos.x <= 0 + radius and velocity_x < 0) or (pos.x >= (screen_size.x - radius) and velocity_x > 0)):
		velocity_x = -velocity_x * BOUNCINESS
		
	elif (velocity_x > DECEL):
		velocity_x -= DECEL
		
	elif (velocity_x < -DECEL):
		velocity_x += DECEL
		
	else:
		velocity_x = 0
	
	
	# return to normal rotation
	if (rotation > ROT_DECEL):
		rotation -= ROT_DECEL
		
	elif (rotation < -ROT_DECEL):
		rotation += ROT_DECEL
		
	else:
		rotation = 0
	
	
	# actually move
	rot_delta = rotation - rot_prev
	
	rotate(-rot_delta)
	move(Vector2(velocity_x * velocity_multiplier, 0))
	
	rot_prev = rotation


func _on_touch_release():
	
	direction = -direction


func hm_mobile(pos):
	
	# accelerate and rotate when pressing screen, depending on direction of travel
	
	#uncommment this for single-press control scheme:
	#if !has_changed_direction:
	#	direction = -direction
	#	has_changed_direction = true
	
	if is_moving and pos.x > 0 and direction == LEFT:
		if (velocity_x > 0):
			velocity_x -= BRAKE * ACCEL
		else:
			velocity_x -= ACCEL
		
		if (rotation > -MAX_ROT):
			rotation -= ROT_ACCEL
	
	if is_moving and pos.x > 0 and direction == RIGHT:
		if (velocity_x < 0):
			velocity_x += BRAKE * ACCEL
		else:
			velocity_x += ACCEL
		
		if (rotation < MAX_ROT):
			rotation += ROT_ACCEL


func hm_keyboard(pos):
	
	# accelerate and rotate when pressing left or right
	if (Input.is_action_pressed("ui_left") and pos.x > 0):
		if (velocity_x > 0):
			velocity_x -= BRAKE * ACCEL
		else:
			velocity_x -= ACCEL
		
		if (rotation > -MAX_ROT):
			rotation -= ROT_ACCEL
	
	if (Input.is_action_pressed("ui_right") and pos.x < screen_size.x):
		if (velocity_x < 0):
			velocity_x += BRAKE * ACCEL
		else:
			velocity_x += ACCEL
		
		if (rotation < MAX_ROT):
			rotation += ROT_ACCEL
	
	
	# prevent over-rotating
	if (!(Input.is_action_pressed("ui_right")) and (rotation > MAX_ROT)):
		rotation = MAX_ROT
		
	elif (!(Input.is_action_pressed("ui_left")) and (rotation < -MAX_ROT)):
		rotation = -MAX_ROT