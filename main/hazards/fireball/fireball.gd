
extends KinematicBody2D

var min_bar_length = 2
var max_bar_length = 4
var fireball_texture_size
var screen_size
var rot_speed = PI * 0.6    #rotations per seconds

export var bar_length = 0


func _ready():
	
	screen_size = get_viewport_rect().size
	fireball_texture_size = get_node("Center/Sprite").get_texture().get_size()
	
	initialize_bar()
	
	set_process(true)


func _process(delta):
	
	rotate(rot_speed * delta)


func _on_balloon_enter():
	
	get_tree().set_pause(true)
	get_parent().get_parent().get_parent().get_node("HUD/Pause").show()


# Creates a fire bar of a random length
func initialize_bar():
	
	# add random number of bars
	randomize()
	var length = (randi() % (max_bar_length - min_bar_length + 1)) + min_bar_length
	bar_length = length
	
	for i in range(length):
	
		var pos = i + 1
		
		#to the left
		var new_left = get_node("Center").duplicate()
		add_child(new_left)
		new_left.set_name("Left " + str(pos))
		new_left.set_pos(Vector2(-pos * fireball_texture_size.x, 0))
		
		#to the right
		var new_right = get_node("Center").duplicate()
		add_child(new_right)
		new_right.set_name("Right " + str(i))
		new_right.set_pos(Vector2(pos * fireball_texture_size.x, 0))
	
	set_pos(Vector2(screen_size.x/2, screen_size.y/2))
	
	# set random rotation
	randomize()
	var random_rotation = randf() * 2 * PI
	rotate(random_rotation)