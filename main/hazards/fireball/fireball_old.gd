
extends Node2D

const FIREBALL_WIDTH_AS_PCT = 0.25 * 0.25     # a quarter of the balloon size (i.e. one sixteenth)

var max_bar_length = 3
var fireball_texture_size
var screen_size
var rot_speed = PI / 2    #rotations per seconds


func _ready():
	
	screen_size = get_viewport_rect().size
	fireball_texture_size = get_node("Center/Sprite").get_texture().get_size()
	
	initialize_bar()
	
	set_process(true)


func _process(delta):
	
	rotate(rot_speed * delta)


# Creates a fire bar of a random length
func initialize_bar():
	
	print("Initializing Bar...")
	# add random number of bars
	randomize()
	var length = (randi() % max_bar_length) + 2
	print(length)
	
	for i in range(length):
		var pos = i + 1
		#to the left
		var new_left = get_node("Center").duplicate()
		add_child(new_left)
		new_left.set_name("Left " + str(i))
		new_left.set_pos(Vector2(-pos * fireball_texture_size.x, 0))
		
		#to the right
		var new_right = get_node("Center").duplicate()
		add_child(new_right)
		new_right.set_name("Right " + str(i))
		new_right.set_pos(Vector2(pos * fireball_texture_size.x, 0))
	
	set_pos(Vector2(screen_size.x/2, screen_size.y/2))