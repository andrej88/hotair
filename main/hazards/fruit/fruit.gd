extends KinematicBody2D

var random_scale = 1
var min_scale = 1.25
var max_scale = 0.75

var random_rot_speed = PI
var min_rot_speed = 0
var max_rot_speed = 2 * PI

var texture


func _ready():
	
	var theme
	
	if get_parent().get_parent():
		theme = get_parent().get_parent().get_parent().theme
	else:
		theme = "deciduous"
	
	randomize()
	if theme == "deciduous":
		var i = randi() % 3
		texture = load("res://main/hazards/fruit/textures/acorn" + str(i + 1) + ".tex")
	elif theme == "boreal":
		var i = randi() % 2
		texture = load("res://main/hazards/fruit/textures/pinecone" + str(i + 1) + ".tex")
	elif theme == "tropical":
		var i = randi() % 3
		texture = load("res://main/hazards/fruit/textures/durian" + str(i + 1) + ".tex")
	else:
		print("Error: theme invalid")
		texture = load("res://main/hazards/fruit/textures/acorn1.tex")
	
	get_node("Sprite").set_texture(texture)
	
	random_scale = rand_range(min_scale, max_scale)
	scale(Vector2(random_scale, random_scale))
	
	var rot_direction = 1
	if (randi() % 2 == 0):
		rot_direction = -1
	
	random_rot_speed = rand_range(min_rot_speed, max_rot_speed) * rot_direction
	
	set_fixed_process(true)


func _fixed_process(delta):
	
	rotate(random_rot_speed * delta)