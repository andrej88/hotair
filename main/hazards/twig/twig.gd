extends KinematicBody2D

var random_scale = 1.0
var min_scale = 0.5
var max_scale = 1.25

var random_rot_speed = PI
var min_rot_speed = 0.5 * PI
var max_rot_speed = 2 * PI

var texture
var shape


func _ready():
	
	var theme
	var shape = get_node("Area2D").get_shape(0)
	
	if get_parent().get_parent():
		theme = get_parent().get_parent().get_parent().theme
	else:
		theme = "deciduous"
	
	randomize()
	if theme != "tropical":
		var i = randi() % 3
		var path = "res://main/hazards/twig/textures/" + theme + str(i + 1) + ".tex"
		texture = load(path)
	else:
		texture = load("res://main/hazards/twig/textures/deciduous1.tex")
	
	get_node("Sprite").set_texture(texture)
	
	random_scale = rand_range(min_scale, max_scale)
	scale(Vector2(random_scale, random_scale))
	
	var rot_direction = 1
	if (randi() % 2 == 0):
		rot_direction = -1
	
	random_rot_speed = rand_range(min_rot_speed, max_rot_speed) * rot_direction
	
	shape.set_radius(texture.get_height() * 0.25)
	shape.set_height(texture.get_width() - 0.5 * texture.get_height())
	
	set_fixed_process(true)


func _fixed_process(delta):
	
	rotate(random_rot_speed * delta)