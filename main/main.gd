extends Node2D

# INITIALIZATION
const BALLOON_TEXTURE_SIZE = Vector2(128,180)
const BALLOON_WIDTH_AS_PCT = 0.25

var screen_size
export var initial_balloon_pos = Vector2(0.5, 0.8)
var scale_factor = 1
var balloon_y_pos

var initialized_score = false
var initialized_game_over = false
var initialized_glass = false
export var theme = ""
var themes = ["deciduous", "boreal", "tropical"]
var theme_chosen = false


# PACKED SCENES
var balloon_pck


# NODES
var hud = get_node("HUD")
var menu = get_node("Menu")
var debug = get_node("HUD/Debug")
var score_node = get_node("HUD/Score")
var score_label = get_node("HUD/Score/Score Label")
var game_over_scale = get_node("HUD/Game Over Scale")
var game_over = get_node("HUD/Game Over Scale/Game Over")
var final_score = get_node("HUD/Game Over Scale/Game Over/Final Score")
var balloon = get_node("Balloon")
var hazards = get_node("Hazards")
var fireballs = get_node("Hazards/Fireballs")
var glass = get_node("Glass")
var background = get_node("Background")


# PROCESS
var time_til_fireball_spawn = 60
var fireball_scroll_speed = 8
var fireballs_passed = 0
var fireball_delay = 100 # initial delay in physics frames until next spawn
const MIN_FIREBALL_DELAY = 50

var fruit_info = {	"node":get_node("Hazards/Fruits"),
					"spawns_from":0,
					"spawns_until":50,
					"spawn_delay":80,
					"spawn_timer":80,
					"speed":8,
					"num_passed":0,
					"min_delay":50,
					"pck":PackedScene.new(),
					"pos_func":"fruit_random_x"}

var twig_info = {	"node":get_node("Hazards/Twigs"),
					"spawns_from":0,
					"spawns_until":50,
					"spawn_delay":80,
					"spawn_timer":80,
					"speed":8,
					"num_passed":0,
					"min_delay":50,
					"pck":PackedScene.new(),
					"pos_func":"fruit_random_x"}

var score = 0

# Save
var save_path = "user://hotair.sav"
var records = {"Music":true, "Sound":true, "Current Balloon":1, "Unlocked Balloons":["RedYellow"], "Previous Scores":[], "High Score":0, "Games Played":0}
var save_initialized = false


func _ready():
	
	randomize()
	if !theme_chosen:
		theme = themes[randi() % themes.size()]
		theme_chosen = true
		print("Theme: " + theme)
	
	# Load packed scenes
	balloon_pck = load("res://main/balloon/balloon.scn")
	fruit_info["pck"] = load("res://main/hazards/fruit/fruit.scn")
	twig_info["pck"] = load("res://main/hazards/twig/twig.scn")
	
	# Get node references
	hud = get_node("HUD")
	menu = get_node("Menu")
	debug = hud.get_node("Debug")
	score_node = hud.get_node("Score")
	score_label = score_node.get_node("Score Label")
	game_over_scale = hud.get_node("Game Over Scale")
	game_over = game_over_scale.get_node("Game Over")
	final_score = game_over.get_node("Final Score")
	hazards = get_node("Hazards")
	fireballs = hazards.get_node("Fireballs")
	fruit_info["node"] = hazards.get_node("Fruits")
	twig_info["node"] = hazards.get_node("Twigs")
	glass = get_node("Glass")
	background = get_node("Background")
	
	score = 0
	
	screen_size = get_viewport_rect().size
	
	# Initialize save file
	if !save_initialized:
		initialize_save()
	
	# Initialize nodes
	scale_factor = 1
	initialize_balloon()
	menu.connect_buttons()
	balloon = get_node("Balloon")
	initialize_hud()
	
	if !initialized_glass:
		initialize_glass()
		initialized_glass = true
	
	menu._ready()
	menu.refresh_balloons()
	background._ready()
	
	fireball_scroll_speed = screen_size.y * 0.01
	
	balloon_y_pos = balloon.get_pos().y
	
	# Start processing, start game
	set_fixed_process(true)
	set_process_input(true)
	
	debug.get_node("1").set_text("D = Debug, R = Reset, C = Skin")
	
	get_tree().set_pause(true)


func initialize_glass():
	glass.scale(Vector2(screen_size.x, screen_size.y))
	if glass.get_pos() == Vector2(0,0):
		glass.set_pos(Vector2(screen_size.x/2, screen_size.y/2))


func initialize_hud():
	
	
	# Initialize Score
	if !initialized_score :
		initialize_gui_item(score_node, score_label, 0.18, Vector2(screen_size.x * 0.05, screen_size.x * 0.05))
		initialized_score = true
		
	score_label.set_text(str(score))
	
	# Initialize Game Over
	if !initialized_game_over :
		initialize_gui_item(game_over_scale, game_over, 0.85, Vector2(screen_size.x * 0.5, screen_size.y * 0.5))
		initialized_game_over = true


func initialize_gui_item(item, size_item, target_width_pct, target_pos):
	
	var target_width = target_width_pct * screen_size.x
	var width = size_item.get_size().x
	var scale = target_width / width
	item.scale(Vector2(scale, scale))
	item.set_pos(target_pos)


func initialize_balloon():
	
	balloon = balloon_pck.instance()
	add_child(balloon)
	move_child(balloon, 2)
	balloon.set_name("Balloon")
	
	scale_factor = 1 / scale_factor
	balloon.scale(Vector2(scale_factor, scale_factor))
	
	var target_balloon_width = BALLOON_WIDTH_AS_PCT * screen_size.x
	var balloon_ratio = BALLOON_TEXTURE_SIZE.y / BALLOON_TEXTURE_SIZE.x
	scale_factor = target_balloon_width / BALLOON_TEXTURE_SIZE.y
	
	# move balloon and set its size
	balloon.set_pos(screen_size * initial_balloon_pos)
	balloon.scale(Vector2(scale_factor, scale_factor))
	
	var radius = balloon.get_node("Sprite").get_texture().get_size().x / 2
	
	balloon.rotate(balloon.rotation)
	balloon.rotation = 0
	
	# Add collision shapes for envelope and basket, respectively
	
	balloon.clear_shapes()
	balloon.add_shape(CircleShape2D.new(), Matrix32(Vector2(1,0), Vector2(0,1), Vector2(0, ((-13.0/32.0) * radius))))
	balloon.add_shape(RectangleShape2D.new(), Matrix32(Vector2(1,0), Vector2(0,1), Vector2(0, ((81.0/64.0) * radius))))
	# USE CircleShape2D.new() NOT JUST CircleShape2D
	
	var envelope = balloon.get_shape(0)
	var basket = balloon.get_shape(1)
	
	envelope.set_radius(radius)
	basket.set_extents(Vector2(13, 9))


func initialize_save():
	
	var f = File.new()
	
	if !f.file_exists(save_path):
		var err = f.open_encrypted_with_pass(save_path, File.WRITE, OS.get_unique_ID())
		if f.is_open():
			f.store_var(records)
			f.close()
	
	else:
		var stored_records
		var err = f.open_encrypted_with_pass(save_path, File.READ, OS.get_unique_ID())
		if f.is_open():
			stored_records = f.get_var()
			f.close()
		var srk = stored_records.keys()
		srk.sort()
		var rk = records.keys()
		rk.sort()
		if srk == rk:
			print("Keys are correct!")
		else:
			print("Keys differ, fixing keys...")
			records = fix_records(stored_records, records)
			var err = f.open_encrypted_with_pass(save_path, File.WRITE, OS.get_unique_ID())
			if f.is_open():
				f.store_var(records)
				f.close()
		
		print("Save file already exists.")
	
	save_initialized = true


func fix_records(old, new):
	
	var old_values = []
	
	for v in old:
		old_values.append(old[v])
	
	var i = 0
	for v in new:
		new[v] = old_values[i]
		i += 1
		if i >= old_values.size():
			break
	
	return new

########################################################
func _fixed_process(delta):
	
	if score >= fruit_info["spawns_from"] and score < fruit_info["spawns_until"]:
		fruit_info["spawn_timer"] -= 1
		if fruit_info["spawn_timer"] <= 0:
			if (randi() % 5) == 0:
				spawn_hazard(twig_info)
			else:
				spawn_hazard(fruit_info)
			fruit_info["spawn_timer"] = fruit_info["spawn_delay"]
			
	handle_hazard(fruit_info)
	handle_hazard(twig_info)


func fruit_random_x():
	
	var pos
	randomize()
	
	if (score < 20):
		pos = randi() % int(screen_size.x)
	else:
		pos = (randi() % int(screen_size.x * 0.5)) + (screen_size.x * 0.15)
	
	return pos


func spawn_hazard(hazard_info):
	
	var new_hazard = hazard_info["pck"].instance()
	hazard_info["node"].add_child(new_hazard)
	
	new_hazard.scale(Vector2(1,1) * scale_factor)
	new_hazard.set_pos(Vector2(call(hazard_info["pos_func"]), -screen_size.y))
	
	new_hazard.get_node("Area2D").connect("body_enter", self, "_on_hazard_body_enter")


func _on_hazard_body_enter(body):
	if (body.get_name() == "Balloon"):
		game_over()


func handle_hazard(hazard_info):
	
	var hazard_count = hazard_info["node"].get_child_count()
	
	for i in range(0, hazard_count):
		
		var current_hazard = hazard_info["node"].get_child(i)
		if current_hazard extends KinematicBody2D:
			current_hazard.move(Vector2(0, hazard_info["speed"]))
			
			if (!(current_hazard.is_in_group("Passed")) and (current_hazard.get_pos().y > screen_size.y)):
				score += 1
				hazard_info["num_passed"] += 1
				
				if (hazard_info["spawn_delay"] > hazard_info["min_delay"]):
					hazard_info["spawn_delay"] -= 1
				
				score_label.set_text(str(score))
				current_hazard.add_to_group("Passed")
			
			if (current_hazard.get_pos().y > screen_size.y * 1.3):
				current_hazard.queue_free()


func game_over():
	
	var f = File.new()
	
	var err = f.open_encrypted_with_pass(save_path, File.READ, OS.get_unique_ID())
	if f.is_open():
		records = f.get_var()
		f.close()
	
	if (score > records["High Score"]):
		print("New High Score!")
		#TODO Print a notification onto the screen
		records["High Score"] = score
	
	records["Games Played"] += 1
	
	var scores = records["Previous Scores"]
	if scores.size() > 50:
		scores.remove(0)
	scores.append(score)
	
	var err = f.open_encrypted_with_pass(save_path, File.WRITE, OS.get_unique_ID())
	if f.is_open():
		f.store_var(records)
		f.close()
	
	final_score.get_node("Label").set_text(str(score))
	game_over_scale.show()
	game_over._ready()
	game_over.set_fixed_process(true)
	glass.show()
	get_tree().set_pause(true)


func _input(event):
	
	# toggle debug node ### DEBUG
	
	if (event.type == InputEvent.KEY and event.unicode == KEY_D + 32):
		if (debug.is_hidden()):
			debug.show()
		else:
			debug.hide()