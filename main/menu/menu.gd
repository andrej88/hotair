extends Node2D

var screen_size
var main = get_parent()
var balloon
var glass
var title = get_node("Title")
var play = get_node("Play")
var records = get_node("Records")
var highscore = get_node("High Score")
var left = get_node("Left")
var right = get_node("Right")
var sound = get_node("Sound")
var music = get_node("Music")
var rec_menu = get_node("Records Menu")
var rec_labels = get_node("Records Menu/Labels")
var rec_label_hs = get_node("Records Menu/Labels/High Score")
var rec_label_as = get_node("Records Menu/Labels/Average Score")
var rec_label_gp = get_node("Records Menu/Labels/Games Played")
var gallery = get_node("Records Menu/Gallery")

var sound_0
var sound_0_p
var sound_1
var sound_1_p

var music_0
var music_0_p
var music_1
var music_1_p

var sound_on
var music_on

var buttons_initialized = false
var records_initialized = false

var texture_index = 1
var texture_dict = { 1:"RedYellow", 2:"Pear", 3:"Lemon", 4:"Montgolfier", 5:"Lightbulb" }
#IDEAS: 5:"ShinyRedBalloon", 6:"Parasail", 7:"Lightbulb", 8:"Jellyfish", 9:"HouseFromUp", 10:"NegativeXray"
# flag balloons? yes.
var unlocked_skins = []
var unlocked_textures = []
var locked_tex

var menu_pos = 0

var save_path = "user://hotair.sav"


func _ready():
	
	main = get_parent()
	balloon = main.get_node("Balloon")
	glass = main.get_node("Glass")
	title = get_node("Title")
	play = get_node("Play")
	records = get_node("Records")
	highscore = get_node("High Score")
	left = get_node("Left")
	right = get_node("Right")
	sound = get_node("Sound")
	music = get_node("Music")
	rec_menu = get_node("Records Menu")
	rec_labels = get_node("Records Menu/Labels")
	rec_label_hs = get_node("Records Menu/Labels/High Score")
	rec_label_as = get_node("Records Menu/Labels/Average Score")
	rec_label_gp = get_node("Records Menu/Labels/Games Played")
	gallery = get_node("Records Menu/Gallery")
	
	screen_size = get_viewport_rect().size
	
	var f = File.new()
	var err = f.open_encrypted_with_pass(save_path, File.READ, OS.get_unique_ID())
	if f.is_open():
		var records_save = f.get_var()
		sound_on = records_save["Sound"]
		music_on = records_save["Music"]
		unlocked_skins = records_save["Unlocked Balloons"]
		f.close()
	
	# Load textures
	sound_0 = load("res://main/menu/textures/sound_0.tex")
	sound_0_p = load("res://main/menu/textures/sound_0_p.tex")
	sound_1 = load("res://main/menu/textures/sound_1.tex")
	sound_1_p = load("res://main/menu/textures/sound_1_p.tex")
	music_0 = load("res://main/menu/textures/music_0.tex")
	music_0_p = load("res://main/menu/textures/music_0_p.tex")
	music_1 = load("res://main/menu/textures/music_1.tex")
	music_1_p = load("res://main/menu/textures/music_1_p.tex")
	###############
#	if unlocked_skins.size() > 0:
#		for i in unlocked_skins:
#			unlocked_textures.append(load("res://main/balloon/textures/" + i +".tex"))
#	else:
#		unlocked_textures.append(load("res://main/balloon/textures/RedYellow.tex"))
#	locked_tex = load("res://main/balloon/textures/locked.tex")
	###############
	
	var records
	var skin = 1
	var f = File.new()
	var err = f.open_encrypted_with_pass(save_path, File.READ, OS.get_unique_ID())
	if f.is_open():
		records = f.get_var()
		f.close()
		skin = records["Current Balloon"]
	
	set_pos(Vector2(screen_size.x / 2, 0))
	
	initialize_title()
	
	if !buttons_initialized:
		initialize_buttons()
		buttons_initialized = true
	
	set_sound_texture()
	set_music_texture()
	
	if !records_initialized:
		initialize_records()
		records_initialized = true
	
	set_process_input(true)


func initialize_title():
	
	var target_width = 0.8 * screen_size.x
	var scale_factor = title.get_scale().x
	var target_scale = target_width / (scale_factor * title.get_texture().get_size().x)
	title.scale(Vector2(target_scale, target_scale))
	title.set_pos(Vector2(0, screen_size.y * 0.2))


func initialize_records():
	
	rec_menu.set_pos(Vector2(screen_size.x * 0.5, 0))
	#Initialize labels
	var labels_width = rec_label_hs.get_node("Label").get_margin(MARGIN_RIGHT) - rec_label_hs.get_margin(MARGIN_LEFT)
	var target_width = screen_size.x * 0.9
	var scale = target_width / labels_width
	var labels_height = scale * rec_label_hs.get_size().y * 3
	rec_labels.scale(Vector2(scale, scale))
	rec_labels.set_pos(Vector2(screen_size.x * 0.05, screen_size.x * 0.05))
	
	#Initialize Gallery
	gallery.set_pos(Vector2(0, labels_height * 1.2))
	var gallery_width = gallery.get_node("Container").get_size().x
	var gallery_target_width = screen_size.x
	var scale = gallery_target_width / gallery_width
	gallery.scale(Vector2(scale, scale))


func refresh_balloons():
	
	var f = File.new()
	var err = f.open_encrypted_with_pass(save_path, File.READ, OS.get_unique_ID())
	if f.is_open():
		var records_save = f.get_var()
		unlocked_skins = records_save["Unlocked Balloons"]
		f.close()
	
	unlocked_textures.clear()
	if unlocked_skins.size() > 0:
		for i in unlocked_skins:
			unlocked_textures.append(load("res://main/balloon/textures/" + i +".tex"))
	else:
		unlocked_textures.append(load("res://main/balloon/textures/RedYellow.tex"))
	locked_tex = load("res://main/balloon/textures/locked.tex")
	
	var gallery_balloons = gallery.get_node("Container").get_children()
	for i in gallery_balloons:
		if i extends Sprite:
			gallery.remove_child(i)
			i.queue_free()
	
	for i in range(texture_dict.size()):
		var b = Sprite.new()
		var index = unlocked_skins.find(texture_dict[i+1])
		if index != -1:
			b.set_texture(unlocked_textures[index])
		else:
			b.set_texture(locked_tex)
		gallery.get_node("Container").add_child(b)
		
		var spacing = 16
		var target_width = 80.0
		var width = b.get_texture().get_size().x
		var height = b.get_texture().get_size().y
		var scale = target_width / width
		var target_height = scale * b.get_texture().get_size().y
		b.scale(Vector2(scale, scale))
		var x = ((i%4) * target_width) + (((i%4)+1) * spacing) + (target_width * 0.5)
		var y = (i/4) * target_height + (((i/4)+1) * spacing) + (target_height * 0.5)
		b.set_pos(Vector2(x,y))
		#b.set_pos(Vector2(((i%4) * 128) + (b.get_texture().get_size().x * b.get_scale().x), ((i/4) * 180) + (b.get_texture().get_size().y * b.get_scale().y)))


func initialize_buttons():
	
	var margin = screen_size.x * 0.08
	var play_y_pos = 0.5 * screen_size.y #0.417
	var hs_r_y_pos = 0.5 * screen_size.y #0.58
	var s_m_margin = 0.03 * screen_size.x
	
	#play
	initialize_menu_item(play, 0.18, Vector2(0,0))
	var new_play_width = play.get_texture().get_size().x * play.get_scale().x
	play.set_pos(Vector2(-0.5 * new_play_width, play_y_pos))
	
	#records
	initialize_menu_item(records, 0.2, Vector2(0,0))
	var new_records_width = records.get_texture().get_size().x * records.get_scale().x
	records.set_pos(Vector2((screen_size.x * 0.5) - margin - new_records_width, hs_r_y_pos))
	
	#highscore
	initialize_menu_item(highscore, 0.18, Vector2(margin - (screen_size.x * 0.5), hs_r_y_pos))
	
	#left
	initialize_menu_item(left, 0.14, Vector2(0,0))
	var new_left_width = left.get_texture().get_size().x * left.get_scale().x
	left.set_pos(Vector2((-0.18 * screen_size.x) - (new_left_width * 0.5), 0.76 * screen_size.y))
	
	#right
	initialize_menu_item(right, 0.14, Vector2(0,0))
	var new_right_width = right.get_texture().get_size().x * right.get_scale().x
	right.set_pos(Vector2((0.18 * screen_size.x) - (new_right_width * 0.5), 0.76 * screen_size.y))
	
	#sound
	initialize_menu_item(sound, 0.1, Vector2(0,0))
	var new_sound_width = sound.get_texture().get_size().x * sound.get_scale().x
	sound.set_pos(Vector2((0.5 * screen_size.x) - s_m_margin - new_sound_width, screen_size.y - s_m_margin - new_sound_width))
	
	#music
	initialize_menu_item(music, 0.1, Vector2(0,0))
	var new_music_width = music.get_texture().get_size().x * music.get_scale().x
	music.set_pos(Vector2((0.5 * screen_size.x) - s_m_margin - new_music_width - s_m_margin - new_sound_width, screen_size.y - s_m_margin - new_music_width))


func initialize_menu_item(item, width_pct, pos):
	
	var target_width = width_pct * screen_size.x
	var width = item.get_texture().get_size().x
	var scale = target_width / width
	item.scale(Vector2(scale, scale))
	var new_width = width * scale
	item.set_pos(pos)


func connect_buttons():
	var balloon = get_parent().get_node("Balloon")
	left.connect("pressed", self, "cycle_skins_left")
	right.connect("pressed", self, "cycle_skins_right")


func _on_Start_released():
	
	hide()
	glass.hide()
	get_parent().get_node("HUD/Score").show()
	get_tree().set_pause(false)
	set_process_input(false)


func _input(event):
	
	if event.type == InputEvent.KEY:
		
		if event.unicode == KEY_S + 32:
			_on_Start_released()
		
		elif event.unicode == KEY_C + 32:
			cycle_skins()
		
		elif event.unicode == KEY_R + 32:
			_on_Records_released()
		
		elif event.unicode == KEY_P + 32:
			cycle_skins_right()
		
		elif event.unicode == KEY_O + 32:
			cycle_skins_left()


func _on_Sound_released():
	
	sound_on = !sound_on
	set_sound_texture()
	
	var f = File.new()
	var err = f.open_encrypted_with_pass(save_path, File.READ, OS.get_unique_ID())
	if f.is_open():
		var stored_records = f.get_var()
		f.close()
		stored_records["Sound"] = sound_on
		var err = f.open_encrypted_with_pass(save_path, File.WRITE, OS.get_unique_ID())
		if f.is_open():
			f.store_var(stored_records)
			f.close()


func _on_Music_released():
	
	music_on = !music_on
	set_music_texture()
	
	var f = File.new()
	var err = f.open_encrypted_with_pass(save_path, File.READ, OS.get_unique_ID())
	if f.is_open():
		var stored_records = f.get_var()
		f.close()
		stored_records["Music"] = music_on
		var err = f.open_encrypted_with_pass(save_path, File.WRITE, OS.get_unique_ID())
		if f.is_open():
			f.store_var(stored_records)
			f.close()


func set_sound_texture():
	if sound_on:     
		print("Sound on, setting sound button textures to \"On\"")
		sound.set_texture(sound_1)
		sound.set_texture_pressed(sound_1)
	else:
		print("Sound off, setting sound button textures to \"Off\"")
		sound.set_texture(sound_0)
		sound.set_texture_pressed(sound_0_p)


func set_music_texture():
	if music_on:
		music.set_texture(music_1)
		music.set_texture_pressed(music_1_p)
	else:
		music.set_texture(music_0)
		music.set_texture_pressed(music_0_p)


func cycle_skins(mode=0):
	
	var texture
	var num_skins
	if unlocked_skins.size() == 0:
		num_skins = 1
	else:
		num_skins = get_largest_skin_index()
	print("NUM SKINS: " + str(num_skins))
	
	#cycle forwards
	if (mode == 0):
		texture_index += 1
		if (texture_index > num_skins):
			texture_index = 1
	
	#cycle backwards
	elif (mode == -1):
		texture_index -= 1
		if (texture_index < 1):
			texture_index = num_skins
	
	# Set skin to texture_dict[mode]
	else:
		if (mode >= num_skins + 1):
			mode = num_skins + 1
		texture_index = mode
	
	var index_of_skin = unlocked_skins.find(texture_dict[texture_index])
	
	if index_of_skin >= 0:
		texture = unlocked_textures[index_of_skin]
		print("Skin " + texture_dict[texture_index] + " is unlocked")
		
		#Save current skin
		var f = File.new()
		var err = f.open_encrypted_with_pass(save_path, File.READ, OS.get_unique_ID())
		if f.is_open():
			var records = f.get_var()
			f.close()
			records["Current Balloon"] = texture_index
			var err = f.open_encrypted_with_pass(save_path, File.WRITE, OS.get_unique_ID())
			if f.is_open():
				f.store_var(records)
				f.close()
	else:
		print("Skin " + texture_dict[texture_index] + " is NOT unlocked")
		texture = locked_tex
	
	balloon.get_node("Sprite").set_texture(texture)


func cycle_skins_left():
	cycle_skins(-1)

func cycle_skins_right():
	cycle_skins(0)


func get_largest_skin_index():
	
	var max_index = 1
	
	for i in texture_dict:
		if unlocked_skins.find(texture_dict[i]):
			max_index = i
	
	return max_index

func _on_Records_released():
	
	var r
	var f = File.new()
	var err = f.open_encrypted_with_pass(save_path, File.READ, OS.get_unique_ID())
	if f.is_open():
		r = f.get_var()
	
	refresh_balloons()
	
	rec_label_hs.get_node("Label").set_text(str(r["High Score"]))
	var avg = 0
	var count = 0
	var total = 0.0
	var pscores = r["Previous Scores"]
	for i in pscores:
		total += i
		count += 1
	if count > 0:
		avg = total/count
	rec_label_as.get_node("Label").set_text(str(abs(round(avg))))
	rec_label_gp.get_node("Label").set_text(str(r["Games Played"]))
	
	if menu_pos == 0:
		slide_screen(1)
		get_tree().set_auto_accept_quit(false)
		menu_pos = 0.5
		play.disconnect("released", self, "_on_Start_released")
	


func _notification(p_notification):
	if p_notification == MainLoop.NOTIFICATION_WM_QUIT_REQUEST and menu_pos == 1:
		
		slide_screen(-1)
		menu_pos = 0.5


func slide_screen(dir):
	
	var scroll_time = 20
	var pos_original = get_pos()
	var balloon_pos_original = balloon.get_pos()
	for i in range(scroll_time):
#		var pos_relative = Vector2(lerp(0, -dir, pow(sin((i+1)/float(scroll_time) * (PI/2)), 2.0)) * screen_size.x, 0)
		var pos_relative = Vector2(lerp(0, -dir, ease((i+1)/float(scroll_time), -2.0)) * screen_size.x, 0)
		set_pos(pos_original + pos_relative)
		balloon.set_pos(balloon_pos_original + pos_relative)
		yield(get_tree(), "fixed_frame")
	if dir == 1:
		menu_pos = 1
	elif dir == -1:
		menu_pos = 0
		get_tree().set_auto_accept_quit(true)
		play.connect("released", self, "_on_Start_released")